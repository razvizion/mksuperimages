# MKSuperimages

[![CI Status](http://img.shields.io/travis/Michal Kolo/MKSuperimages.svg?style=flat)](https://travis-ci.org/Michal Kolo/MKSuperimages)
[![Version](https://img.shields.io/cocoapods/v/MKSuperimages.svg?style=flat)](http://cocoapods.org/pods/MKSuperimages)
[![License](https://img.shields.io/cocoapods/l/MKSuperimages.svg?style=flat)](http://cocoapods.org/pods/MKSuperimages)
[![Platform](https://img.shields.io/cocoapods/p/MKSuperimages.svg?style=flat)](http://cocoapods.org/pods/MKSuperimages)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MKSuperimages is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MKSuperimages'
```

## Author

Michal Kolo, kolo111@gmail.com

## License

MKSuperimages is available under the MIT license. See the LICENSE file for more info.
